def diagonalDifference(a):
    n = len(a)
    p = s = 0
    for row in range(n):
        p += a[row][row]
        s += a[row][n - row - 1]

    result = p - s
    return result if result > 0 else (-1 * result)
