from collections import Counter

def migratoryBirds(n, ar):
    # Complete this function

    # Try-2
    counter = [0] * 6
    for el in ar
        counter[el] += 1
    return counter.index(max(counter))

    # Try-1
    # c = Counter(ar)
    # m = c.most_common()
    # count_val = m[0][1]
    # return sorted(filter(lambda x: x[1] == count_val, m))[0][0]

migratoryBirds(6, [1, 4, 4, 4, 5, 3]) # 4
migratoryBirds(6, [1, 4, 4, 4, 5, 3, 3, 3]) # 3
