#!/bin/python3

import sys

def divisibleSumPairs(n, k, ar):
    return sum([1 for i in range(n) for j in range(i + 1, n) if (ar[i] + ar[j]) % k == 0])

def divisibleSumPairs_sloppy_idea1(n, k, ar):
    c = 0
    for i in range(n):
        for j in range(i + 1, n):
            if (ar[i] + ar[j]) % k == 0:
                c += 1
    return c

def divisibleSumPairs_sloppy_idea2(n, k, ar):
    m = [[]] * k
    for i in range(n):
        m[ar[i] % k].append(ar[i])
    return sum([len(i) for i in m])

n, k = input().strip().split(' ')
n, k = [int(n), int(k)]
ar = list(map(int, input().strip().split(' ')))
result = divisibleSumPairs(n, k, ar)
print(result)
