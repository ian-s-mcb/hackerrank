#!/bin/python3

import sys

def kangaroo(x1, v1, x2, v2):
    if v1 > v2 and ((x1 - x2) % (v2 - v1)) == 0:
        return 'YES'
    else:
        return 'NO'


def kangaroo_sloppy_idea1(x1, v1, x2, v2):
    # Complete this function
    oneHasMaxSpeed = v1 > v2
    one = x1
    two = x2
    while True:
        one += v1
        two += v2
        if one == two:
            return 'YES'
        elif (oneHasMaxSpeed and one > two) or (not oneHasMaxSpeed and two > one):
            return 'NO'

def kangaroo_sloppy_idea2(x1, v1, x2, v2):
    num = x1 - x2
    denom = v2 - v1
    if denom < 0:
        f = num / denom
        if f > 0 and f == int(f):
            return 'YES'
    return 'NO'

x1, v1, x2, v2 = input().strip().split(' ')
x1, v1, x2, v2 = [int(x1), int(v1), int(x2), int(v2)]
result = kangaroo(x1, v1, x2, v2)
print(result)
