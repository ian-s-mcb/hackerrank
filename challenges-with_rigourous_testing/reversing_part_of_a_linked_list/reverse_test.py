import unittest
import random

from linked_list import LinkedList
from reverse import reversePartial, reverseFull


class ReverseTest(unittest.TestCase):
    def test_full_small_list(self):
        l = list(range(5))
        expected = list(l)
        expected.reverse()
        self.assertEqual(reverseFull(LinkedList(lst=l)), LinkedList(lst=expected))

    def test_full_big_lists(self):
        trial_count = 20
        for i in range(trial_count):
            l = list([random.randrange(0, 100) for i in range(1000)])
            expected = list(l)
            expected.reverse()
            self.assertEqual(reverseFull(LinkedList(lst=l)), LinkedList(lst=expected))

    def test_full_empty_list(self):
        lst = []
        self.assertEqual(reverseFull(LinkedList(lst=lst)),
            LinkedList(lst=lst))

    def test_partial_small_list(self):
        lst = list(range(5))
        m = 1
        n = 3
        expected = [0, 3, 2, 1, 4]
        self.assertEqual(reversePartial(LinkedList(lst=lst), m, n),
            LinkedList(lst=expected))

    def test_partial_small_list_starting_at_front(self):
        lst = list(range(5))
        m = 0
        n = 2
        expected = [2, 1, 0, 3, 4]
        self.assertEqual(reversePartial(LinkedList(lst=lst), m, n),
            LinkedList(lst=expected))

    def test_partial_small_list_starting_at_back(self):
        lst = list(range(5))
        m = 2
        n = 4
        expected = [0, 1, 4, 3, 2]
        self.assertEqual(reversePartial(LinkedList(lst=lst), m, n),
            LinkedList(lst=expected))

    def test_partial_big_lists(self):
        trial_count = 20
        for i in range(trial_count):
            lst_size = 1000
            lst = list(range(lst_size))
            m = random.randrange(0, lst_size - 1)
            n = random.randrange(m, lst_size)
            portion = lst[m:n + 1]
            portion.reverse()
            expected = lst[:m] + portion + lst[n + 1:]
            self.assertEqual(reversePartial(LinkedList(lst=lst), m, n),
                LinkedList(lst=expected))
