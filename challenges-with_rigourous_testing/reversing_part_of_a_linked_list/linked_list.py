class Node:
    def __init__(self, value = None, next = None):
        self.value = value
        self.next = next

    def __str__(self):
        return 'Node ['+str(self.value)+']'

class LinkedList:
    def __init__(self, lst=None, ll=None):
        if lst and ll:
            raise Exception(
                'LinkedList constructor cannot accept both an list and'
                'linked list')
        if lst == None and ll == None:
            self.first = None
            self.last = None
        elif lst != None:
            length = len(lst)
            if length == 0:
                self.first = self.last = Node()
            elif length == 1:
                self.first = self.last = Node(lst[0], None)
            elif length > 1:
                self.first = previous = Node(lst[0], None)
                for el in lst[1:]:
                    current = Node(el, None)
                    previous.next = current
                    previous = current
                self.last = current
        elif ll != None:
            curr_this = self.first = None
            curr_other = ll.first
            previous = None
            if curr_other != None:
                curr_this = self.first = Node(curr_other.value)
                curr_other = curr_other.next
                while curr_other != None:
                    previous = curr_this
                    curr_this = Node(curr_other.value)
                    previous.next = curr_this
                    curr_other = curr_other.next
            self.last = curr_this


    def insert(self, x):
        if self.first == None:
            self.first = Node(x, None)
            self.last = self.first
        else:
            current = Node(x, None)
            self.last.next = current
            self.last = current

    def __eq__(self, other):
        if other == None:
            return False
        result = True
        curr_this = self.first
        curr_other = other.first
        while (curr_this != None and curr_other != None):
            if curr_this.value != curr_other.value:
                result = False
                break
            curr_this, curr_other = curr_this.next, curr_other.next
        if curr_this != None or curr_other != None:
            result = False
        return result

    def __str__(self):
        result = 'LinkedList['
        current = self.first
        if current != None:
            result += str(current.value)
            current = current.next
        while current != None:
            result += f', {current.value}'
            current = current.next
        result += ']'
        return result
    def __repr__(self):
        return self.__str__()

    def clear(self):
        self.__init__()
