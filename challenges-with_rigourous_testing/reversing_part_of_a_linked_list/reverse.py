# Complete the function below.

#For your reference:
#LinkedListNode {
#    int val
#    LinkedListNode next
#}
#
# Input: 1->2->3->4->5, m = 2, n = 4
# Output: 1->4->3->2->5

from linked_list import LinkedList

def reverseFull(ll):
    ll = LinkedList(ll=ll)
    current = ll.first
    previous = None
    temp = None
    ll.last = current
    while current != None:
        temp = current.next
        current.next = previous
        previous = current
        current = temp
    ll.first = previous
    return ll

def reversePartial(ll, m, n):
    ll = LinkedList(ll=ll)
    current = ll.first
    previous = None
    temp = None
    if current.value == m:
        first = None
    else:
        first = current
    while current.value != m:
        previous = current
        current = current.next
    before = previous
    partial_last = current
    while current.value != n:
        temp = current.next
        current.next = previous
        previous = current
        current = temp
    after = current.next
    current.next = previous
    partial_last.next = after
    if before != None:
        before.next = current
    if first == None:
        first = current
    ll.first = first
    return ll

# Attempt during interview
#
# def reverse(node, m, n):
#     prev = node
#     while prev.next.val != m:
#         prev = prev.next
#     curr = partialTailNode = prev.next
#     while curr.val != n:
#         b1 = curr.next
#         b2 = b1.next
#         b1.next = curr
#         curr = b1
#     partialTailNode.next = b2
#     prev.next = curr
#     return node
