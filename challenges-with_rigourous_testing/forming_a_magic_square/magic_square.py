def formingMagicSquare(s):
    # Complete this function
    ss = [sum(s[i]) for i in range(3)]
    t = [[s[j][i] for j in range(3)] for i in range(3)]
    tt = [sum(t[i]) for i in range(3)]
    target = sorted(ss + tt)[2]
    ss = [el - target for el in ss]
    tt = [el - target for el in tt]
    result = 0
    for sse, tte in zip(ss, tt):
        if sse == tte and sse != 0:
            result += abs(sse)
        elif sse != 0 or tte != 0:
            result += abs(sse) + abs(tte)
    return result
    

print(formingMagicSquare([[4, 9, 2], [3, 5, 7], [8, 1, 5]])) # 1
print(formingMagicSquare([[4, 8, 2], [4, 5, 7], [6, 1, 6]])) # 4
